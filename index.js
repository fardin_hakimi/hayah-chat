
const express = require('express');
var socket = require('socket.io');
var app = express();

// setup server
var server = app.listen(3000, ()=>{
  console.log("Server is listening at port 3000 ");
});

// middleware to serve static files (html, css, images, js and ..)
app.use(express.static('public'));

// set up socket io
var io = socket(server);

io.on("connection", (socket)=>{
  console.log("client connected");

// handles new message event
  socket.on("message",(message)=>{
    // broadcast to all sockets/clients
    io.sockets.emit("message",message);
  });

  socket.on("typing",(data)=>{
    socket.broadcast.emit("typing",data);
  });

});
