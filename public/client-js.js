

var sendButton = $("#send-button");
var message = $("#message");
var username = $("#username");
var feedback = $("#feedback");
var socket = io.connect("http://localhost:3000");

sendButton.on("click",(event)=>{
  // check if a message and a username exists
  if (message.val() && username.val()){
    // emit a message event, which is picked up by the server
    socket.emit("message",{"username":username.val(),"message":message.val()});
    message.val("");
  }
});

// emit typing event
message.on("keypress",(event)=>{
if (username.val()){
  socket.emit("typing",{"username":username.val()});
}
});

// listen to  message event from server
socket.on("message", (data)=>{
    feedback.html("");
    var messageItem = "<li><p>"+data.username+": "+data.message+"</p></li>";
    $("#messagesList").append(messageItem);
});
// listen to typing event from server

socket.on("typing", (data)=>{
     var feedbackItem = ("<p> "+data.username+" is typing a message...</p>");
    feedback.html(feedbackItem);
});
